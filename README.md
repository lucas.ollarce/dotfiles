# Dotfiles

Mis dotfiles

# VIM

## Requerimientos
* [Vim-plug](https://github.com/junegunn/vim-plug)

FZF:
* [Bat](https://github.com/sharkdp/bat)
* [Ripgrep](https://github.com/BurntSushi/ripgrep)
* [The Silver Searcher](https://github.com/ggreer/the_silver_searcher)

Minimap:
* [Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)
* [Code-minimap](https://github.com/wfxr/code-minimap/)

Plantum Preview:
* [Graphviz](https://www.graphviz.org/download/)

Taglist:
* [Universal Ctags](https://github.com/universal-ctags/ctags)

Bracey:
* [Node.js](https://nodejs.org/es/)
* [npm](https://www.npmjs.com/)

DevIcons:
* [Fuente Parcheada de Nerd Fonts](https://github.com/ryanoasis/nerd-fonts)

## Instalación
Crear carpera swpfiles y colocar plugins.vim adentro del directorio .vim
```sh
mkdir ~/.vim/swpfiles
cd dotfiles
cp plugins.vim ~/.vim/
```
Ejecutar
```sh
vim +PlugInstall +qall
```
Esto comenzara la instalación de todos los plugins y cerrara vim al finalizar
