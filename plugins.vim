call plug#begin()

Plug 'junegunn/vim-plug'
Plug 'jmcantrell/vim-virtualenv', { 'for': 'python' }
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }
Plug 'vim-syntastic/syntastic'
Plug 'altercation/vim-colors-solarized'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeToogle', 'NERDTreeFind']}
Plug 'jistr/vim-nerdtree-tabs'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'preservim/nerdcommenter' |
        \ Plug 'Xuyuanp/nerdtree-git-plugin' |
        \ Plug 'ryanoasis/vim-devicons'
Plug 'bryanmylee/vim-colorscheme-icons'
Plug 'bagrat/vim-buffet'
Plug 'spf13/vim-autoclose'
Plug 'luochen1990/rainbow'
Plug 'ervandew/supertab'
Plug 'airblade/vim-gitgutter'
Plug 'codota/tabnine-vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'wfxr/minimap.vim', {'do': ':!cargo install --locked code-minimap'}
Plug 'Yggdroot/indentLine'
Plug 'chrisbra/Colorizer'
Plug 'vim-scripts/taglist.vim'
Plug 'mattn/emmet-vim', { 'for': ['html', 'html5', 'css']}
Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
Plug 'SirVer/ultisnips'
Plug 'KabbAmine/vCoolor.vim'
Plug 'skanehira/preview-uml.vim'
Plug 'aklt/plantuml-syntax'
Plug 'tyru/open-browser.vim'
Plug 'weirongxu/plantuml-previewer.vim'

call plug#end() " required
