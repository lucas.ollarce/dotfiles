"-----------------------------------------------------------------------
"--------------------------------------------------------------NERD Font
" Para que los caracteres de Vim Buffet y DevIcons funcionen hay que
" instalar una fuente parcheada compatible y configurar la fuente en 
" la terminal, en este caso se usa Inconsolata Nerd Font
" 
" https://github.com/ryanoasis/nerd-fonts

" Para usar Taglist debe instalar ctags, si usa una distribución basada
" en Debian para esto ejecute en la terminal:
" sudo apt install universal-ctags

"-----------------------------------------------------------------------
"-----------------------------------------------------------------------
" Ubicacion de archivos de intercambio .swp
set directory=~/.vim/swpfiles//

" Mostrar números de línea
set number

" Habilitar autoidentación
set autoindent

" Habilitar identación por tipo de archivo
filetype indent on

" Encoding
set encoding=UTF-8

" Usar colores 256
"set t_Co=256

" Configurar tecla espacio como <leader> shortcut
nnoremap <Space> <Nop>
let mapleader = " "

" Habilitar complementos de tipo de archivos
filetype plugin on

" Habilitar uso del mouse
set mouse=a

" Compartir portapapeles
set clipboard=unnamedplus

" Divisiones
set splitbelow
set splitright

" Navegacion entre divisiones
" Ctrl+J bajar
nnoremap <C-J> <C-W><C-J>
" Ctrl+K subir
nnoremap <C-K> <C-W><C-K>
" Ctrl+L derecha
nnoremap <C-L> <C-W><C-L>
" Ctrl+H izquierda
nnoremap <C-H> <C-W><C-H>

" Cambiar el tamaño de las divisiones
map <leader>> <C-w>10>
map <leader>< <C-w>10<
map <leader>+ <C-w>5+
map <leader>- <C-w>5-

" Salvado rápido
nmap <leader>s :w<CR>

" Ejecutar código de python
" map <F9> :w<CR>:!clear:python3 %; read<CR>
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!clear; python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!clear; python3' shellescape(@%, 1)<CR>

" Mal espacio en blanco
au BufNewFile, BufRead *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Abrir terminal
map <F12> :belowright terminal<CR>

" Tamaño por defecto del terminal
set termwinsize=10x0

" Modo Terminal-Normal
tnoremap <Esc><Esc> <C-w>N

" Autocompletar etiquetas html
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
:ab <<< </<C-x><C-o>

" Identación de 2 espacios para html
au FileType html setl sw=2 sts=2 et

" No comptible con Vi
set nocompatible

" Resaltar los resultados de la búsqueda
set hlsearch

" Dejar de resaltar los resultados de la busqueda
nmap <silent> ,/ :nohlsearch<CR>

" Recargar archivo cuando se cambia externamente
":au CursorHold * checktime

" Reducir tiempo de actualización
set updatetime=100

" No mostrar una línea en varias lineas si es demasiado larga
set nowrap

"-----------------------------------------------------------------------
"----------------------------------------------------------------Plugins
" Cargar plugins
so ~/.vim/plugins.vim

" Habilitar resaltado de sintaxis
syntax on

"-----------------------------------------------------------------------
"-------------------------------------------------------------Vim Buffet
" Muestra pestañas y buffers combinados en la linea de tabulación

" Mostrar linea de tabulación solo si hay mas de un buffer o pestaña
let g:buffet_always_show_tabline = 1

" Carécter que se utilizará para separar elementos
let g:buffet_separator = "\uf641"

" Usar separadores de powerline entre buffers y pestañas
let g:buffet_powerline_separators = 1

" Mostrar el índice antes de cada nombre de buffer
let g:buffet_show_index = 1

" Mostrar iconos de tipo de archivo para cada buffer con vim-devicons
let g:buffet_use_devicons = 1

" Carácter que se utilizará como icono para las pestañas
let g:buffet_tab_icon = "\uf00a"

" Carácter que se mostrará mendiante el recuento de buffers truncados
" A la izquierda
let g:buffet_left_trunc_icon = "\uf0a8"
" A la derecha
let g:buffet_right_trunc_icon = "\uf0a9"

" Colores para coincidir con el tema de airline powerlish
"function! g:BuffetSetCustomColors()
"" El buffer actual
  "hi! BuffetCurrentBuffer    cterm=bold ctermbg=148 ctermfg=22 guibg=#afd700 guifg=#005f00
"" Un buffer activo (un buffer no actual visible en una ventana no actual)
  "hi! BuffetActiveBuffer     cterm=NONE ctermbg=233 ctermfg=148 guibg=#ffd7af guifg=#afd700
"" Un buffer no actual y no activo 
"hi! BuffetBuffer           cterm=NONE ctermbg=233 ctermfg=15 guibg=#ffd7af guifg=#ffffff
"" El buffer actual cuando se modifica
  "hi! BuffetModCurrentBuffer cterm=bold ctermbg=15 ctermfg=24 guibg=#ffffff guifg=#005f87
"" Un buffer activo modificado (un buffer no actual visible en una ventana no actual)
  "hi! BuffetModActiveBuffer  cterm=NONE ctermbg=24 ctermfg=15 guibg=#005f87 guifg=#ffffff
"" Un buffer no actual y no activo modificado
  "hi! BuffetModBuffer        cterm=NONE ctermbg=233 ctermfg=75 guibg=#ffd7af guifg=#5fafff
"" El indicador de truncamiento (recuento de buffers truncados desde la
"" izquierda o la derecha)
  "hi! BuffetTrunc            cterm=NONE ctermbg=233 ctermfg=166 guibg=#ffd7af guifg=#d75f00
"" Una pestaña
  "hi! BuffetTab              cterm=NONE ctermbg=166 ctermfg=233 guibg=#d75f00 guifg=#ffd7af
"endfunction"""

" Colores para coincidir con el tema de airline violet
function! g:BuffetSetCustomColors()
" El buffer actual
  hi! BuffetCurrentBuffer    cterm=bold ctermbg=97 ctermfg=250 guibg=#875faf guifg=#bcbcbc
" Un buffer activo (un buffer no actual visible en una ventana no actual)
  hi! BuffetActiveBuffer     cterm=NONE ctermbg=239 ctermfg=170 guibg=#4e4e4e guifg=#d75fd7
" Un buffer no actual y no activo 
  hi! BuffetBuffer           cterm=NONE ctermbg=233 ctermfg=35 guibg=#121212 guifg=#00af5f
" El buffer actual cuando se modifica
  hi! BuffetModCurrentBuffer cterm=bold ctermbg=35 ctermfg=253 guibg=#00af5f guifg=#dadada
" Un buffer activo modificado (un buffer no actual visible en una ventana no actual)
  hi! BuffetModActiveBuffer  cterm=NONE ctermbg=253 ctermfg=35 guibg=#dadada guifg=#00af5f
" Un buffer no actual y no activo modificado
  hi! BuffetModBuffer        cterm=NONE ctermbg=233 ctermfg=239 guibg=#121212 guifg=#4e4e4e
" El indicador de truncamiento (recuento de buffers truncados desde la
" izquierda o la derecha)
  hi! BuffetTrunc            cterm=NONE ctermbg=233 ctermfg=166 guibg=#121212 guifg=#d75f00
" Una pestaña
  hi! BuffetTab              cterm=NONE ctermbg=166 ctermfg=233 guibg=#d75f00 guifg=#121212
endfunction


" Reasignación de teclas
" Abrir un nuevo buffer vacío
nmap <leader>T :enew<CR>
" Moverse al siguiente buffer
noremap <Tab> :bn<CR>
" Moverse al buffer anterior
noremap <S-Tab> :bp<CR>
" Cerrar el buffer actual y pasar al anterior
" (Esto replica la idea de cerrar una pestaña)
nmap <leader>bq :bd <BAR> bp # <CR>
" Mostar todos los buffers abiertos y sus estados
nmap <leader>bl :ls<CR>
" Limpiar el buffer actual sin cerrar la ventana
noremap <Leader><Tab> :Bw<CR>
" Forzar limpieza del buffer actual sin guardar cambios
noremap <Leader><S-Tab> :Bw!<CR>
" Abrir una nueva pestaña con una ventana vacía
noremap <C-t> :tabnew split<CR>

" Cambiar entre buffers
nmap <leader>1 <Plug>BuffetSwitch(1)
nmap <leader>2 <Plug>BuffetSwitch(2)
nmap <leader>3 <Plug>BuffetSwitch(3)
nmap <leader>4 <Plug>BuffetSwitch(4)
nmap <leader>5 <Plug>BuffetSwitch(5)
nmap <leader>6 <Plug>BuffetSwitch(6)
nmap <leader>7 <Plug>BuffetSwitch(7)
nmap <leader>8 <Plug>BuffetSwitch(8)
nmap <leader>9 <Plug>BuffetSwitch(9)
nmap <leader>0 <Plug>BuffetSwitch(10)

"-----------------------------------------------------------------------
"--------------------------------------------------------------Solarized
" Si desea usar solarized como esquema de color descomente las
" siguientes lineas y comente las lineas de dracula

"set background=dark
"let g:solarized_termcolors=256
"colorscheme solarized

" Cambiar entre tema light o dark
"call togglebg#map("<F5>")   

"-----------------------------------------------------------------------
"----------------------------------------------------------------Dracula
"Usar colores gui
set termguicolors
let g:dracula_colorterm = 0
colorscheme dracula

"-----------------------------------------------------------------------
"-------------------------------------------------------------Git-gutter
" Mostrar siempre la columna de signos
set signcolumn=yes
" Corregir el problema de contraste de la columna de git-gutter con el 
" tema oscuro de solarized
highlight clear SignColumn

"-----------------------------------------------------------------------
"----------------------------------------------------------------Airline
" Establecer airline tema
let g:airline_powerline_fonts=1
let g:airline_theme='violet'
let g:airline_section_z = airline#section#create(['windowswap', '%3p%% ', 'linenr', ':%3v'])
let g:airline_skip_empty_sections = 10

"-----------------------------------------------------------------------
"-----------------------------------------------------------Vim-DevIcons
" Activar/Desactivar las decoraciones de glifos de nodo de archivo
let g:WebDevIconsUnicodeDecorateFileNodes = 1

" Usar glifos de ancho doble (1) o de ancho simple (0)
let g:WebDevIconsUnicodeGlyphDoubleWidth  =  1

" No mostrar los corchetes [] alrededor de las banderas de NerdTree
let g:webdevicons_conceal_nerdtree_brackets = 1

"-----------------------------------------------------------------------
"---------------------------------------------------------------NERDTree
" Ignorar archivos .pyc en NERDTree
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree

" No dejar que vim abra archivos y otros búferes en la ventana de NerdTree
autocmd BufEnter * if bufname('#') =~# "^NERD_tree_" && winnr('$') > 1 | b# | endif

" Evitar bloqueos al llamar funciones de vim-plug mientras esta en la ventada
" de NerdTree
let g:plug_window = 'noautocmd vertical topleft new'

" Mostrar archivos ocultos
let NERDTreeShowHidden=1

" Abrir/Cerrar NerdTree
nmap <F2> :NERDTreeToggle<CR>
" Abrir NerdTree con el archivo actual seleccionado
nmap <F3> :NERDTreeFind<CR>

"-----------------------------------------------------------------------
"------------------------------------------------FZF integración con Vim
" Runtimepath
set rtp+=~/.fzf
" hen `window` entry is a dictionary, fzf will start in a popup window. The
" following options are allowed:

"- Required:
    "- `width` [float range [0 ~ 1]]
    "- `height` [float range [0 ~ 1]]
"- Optional:
    "- `yoffset` [float default 0.5 range [0 ~ 1]]
    "- `xoffset` [float default 0.5 range [0 ~ 1]]
    "- `highlight` [string default `'Comment'`]: Highlight group for border
    "- `border` [string default `rounded`]: Border style
        "- `rounded` / `sharp` / `horizontal` / `vertical` / `top` / `bottom` / `left` / `right`

let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'border': 'rounded' } }
let $FZF_DEFAULT_OPTS="--exact --preview-window 'right:50%' --margin=1,4"

" Ocultar linea de estado mientras se ejecuta FZF
autocmd! FileType fzf set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler

" Atajos de teclados
" Abrir buscador de archivos para repos git ocultando archivos de .gitignore 
nnoremap <leader>ff :GFiles --cached --others --exclude-standard<CR>
" Abrir buscador de archivos
nnoremap <C-f> :Files<CR>
" Abrir buscador de buffers
nnoremap <leader>b :Buffers<CR>
" Abrir fzf con repgrip
nnoremap <leader>fr :Rg<CR>
" Abrir fzf con the silver searcher
nnoremap <leader>fa :Ag<CR>

"-----------------------------------------------------------------------
"----------------------------------------------------------------Minimap
" Para poder usar minimap debe instalar code-minimap
" (https://github.com/wfxr/code-minimap/), esto se hace a traves de cargo,
" para instalar cargo ejecute en la terminal:
" curl https://sh.rustup.rs -sSf | sh
" Esto descargará un script e iniciará la instalación.
" Si todo va bien, veras esto:
" Rust is installed now. Great!
" Ahora podra instalar code-minimap ejecutando en la terminal:
" cargo install --locked code-minimap

" Deshabilitar el minimapa para tipos de archivos específicos
let g:minimap_block_filetypes = ['fugitive', 'nerdtree', 'tagbar']

" Deshabilitar el minimapa para tipos de buffers específicos
let g:minimap_block_buftypes = ['nofile', 'nowrite', 'quickfix', 'terminal', 'prompt', 'fzf', 'pymode']

" Cerrar minimapa para tipos de archivos especificos
let g:minimap_close_filetypes = ['starify', 'netrw', 'vim-plug', 'vim-blunde']

" Resaltar el rango de lineas visibles
let g:minimap_highlight_range = 1

" Atajos de teclado
" Abrir/Cerrar Minimap
nnoremap <leader>m :MinimapToggle<CR>
" Cerrar Minimap
nnoremap <leader>mq :MinimapClose<CR>
" Refrescar Minimap
nnoremap <leader>mr :MinimapRefresh<CR>

"-----------------------------------------------------------------------
"----------------------------------------------------------------Py-mode
" Comprobación/Resaltado de sintaxis
let python_highlight_all=1

"Mostrar la documentación de la palabra actual mediante pydoc
let g:pymode_doc = 1

" Omitir errores y advertencias de python linea muy larga
let g:pymode_lint_ignore=["E501",]

"-----------------------------------------------------------------------
"----------------------------------------------------------------Rainbow
" Habilitar vim-rainbow globalmente
let g:rainbow_active  =  1

"-----------------------------------------------------------------------
"--------------------------------------------------------------Autoclose
" No emparejar comillas en archivos vim 
let g:autoclose_vim_commentmode = 1

"-----------------------------------------------------------------------
"-------------------------------------------------------------IndentLine
" Cambiar carácter de sangría 
" \ue621 |¦˸։׀׃፧᎒⁝⁞Ⅰⅼ∣⋮⎜⎢⎸⎹ │┃┆┇┊┋▏▕❘❙❙⠇⡇⦙⫶ⵗⵂⵏ⸽⼁〡ㅣ㆐㇑丨꒐ꓲ꘡ꟾ꠰꫰︓︙︱︳｜ￜ￨￤𐌠𐍘𐍭𐑦𐔎𑁇𑃀𑅁𑇅𑑋𑗅𑙁𑩂𑪛𑫥𒑉𒑖𒐕𒑰𓏪𓏺𓏬𖩙𖭐𘠁𛰇𛰌𝄀𝄄𝄄𝅥𝆃𝆄𝆹𝅥𝆺𝅥𝇁𝇂𝍩𝩷𝩸𝩹𝩺𞅁𞣇𞠢𞥑𞸀🭰🭱🭲🭳🭴🭵
" \ue621 |¦˸։׀׃፧᎒⁝⁞Ⅰⅼ∣⋮⎜⎢⎸⎹ │┃┆┇┊┋▏▕❘❙❙⠇⡇⦙⫶ⵗⵂⵏ⸽⼁〡ㅣ㆐㇑丨꒐ꓲ꘡ꟾ꠰꫰︓︙︱︳｜ￜ￨￤𐌠𐍘𐍭𐑦𐔎𑁇𑃀𑅁𑇅𑑋𑗅𑙁𑩂𑪛𑫥𒑉𒑖𒐕𒑰𓏪𓏺𓏬𖩙𖭐𘠁𛰇𛰌𝄀𝄄𝄄𝅥𝆃𝆄𝆹𝅥𝆺𝅥𝇁𝇂𝍩𝩷𝩸𝩹𝩺𞅁𞣇𞠢𞥑𞸀🭰🭱🭲🭳🭴🭵
" \ue621 |¦˸։׀׃፧᎒⁝⁞Ⅰⅼ∣⋮⎜⎢⎸⎹ │┃┆┇┊┋▏▕❘❙❙⠇⡇⦙⫶ⵗⵂⵏ⸽⼁〡ㅣ㆐㇑丨꒐ꓲ꘡ꟾ꠰꫰︓︙︱︳｜ￜ￨￤𐌠𐍘𐍭𐑦𐔎𑁇𑃀𑅁𑇅𑑋𑗅𑙁𑩂𑪛𑫥𒑉𒑖𒐕𒑰𓏪𓏺𓏬𖩙𖭐𘠁𛰇𛰌𝄀𝄄𝄄𝅥𝆃𝆄𝆹𝅥𝆺𝅥𝇁𝇂𝍩𝩷𝩸𝩹𝩺𞅁𞣇𞠢𞥑𞸀🭰🭱🭲🭳🭴🭵
" \ue621 |¦˸։׀׃፧᎒⁝⁞Ⅰⅼ∣⋮⎜⎢⎸⎹ │┃┆┇┊┋▏▕❘❙❙⠇⡇⦙⫶ⵗⵂⵏ⸽⼁〡ㅣ㆐㇑丨꒐ꓲ꘡ꟾ꠰꫰︓︙︱︳｜ￜ￨￤𐌠𐍘𐍭𐑦𐔎𑁇𑃀𑅁𑇅𑑋𑗅𑙁𑩂𑪛𑫥𒑉𒑖𒐕𒑰𓏪𓏺𓏬𖩙𖭐𘠁𛰇𛰌𝄀𝄄𝄄𝅥𝆃𝆄𝆹𝅥𝆺𝅥𝇁𝇂𝍩𝩷𝩸𝩹𝩺𞅁𞣇𞠢𞥑𞸀🭰🭱🭲🭳🭴🭵
" \ue621 |¦˸։׀׃፧᎒⁝⁞Ⅰⅼ∣⋮⎜⎢⎸⎹ │┃┆┇┊┋▏▕❘❙❙⠇⡇⦙⫶ⵗⵂⵏ⸽⼁〡ㅣ㆐㇑丨꒐ꓲ꘡ꟾ꠰꫰︓︙︱︳｜ￜ￨￤𐌠𐍘𐍭𐑦𐔎𑁇𑃀𑅁𑇅𑑋𑗅𑙁𑩂𑪛𑫥𒑉𒑖𒐕𒑰𓏪𓏺𓏬𖩙𖭐𘠁𛰇𛰌𝄀𝄄𝄄𝅥𝆃𝆄𝆹𝅥𝆺𝅥𝇁𝇂𝍩𝩷𝩸𝩹𝩺𞅁𞣇𞠢𞥑𞸀🭰🭱🭲🭳🭴🭵
let g:indentLine_char = "▏"


" Atajo de teclado
nnoremap <leader>i :IndentLinesToggle<CR>

"-----------------------------------------------------------------------
"------------------------------------------------------------------Emmet
"Habilitar solo para html/css
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall

"Atajo de teclado Ctrl+X+,
let g:user_emmet_leader_key = '<C-X>'

"-----------------------------------------------------------------------
"--------------------------------------------------------------UltiSnips
"Orientación al abrir la ventana
let g:UltiSnipsEditSplit = 'vertical'

"Configuración del activador
let g:UltiSnipsExpandTrigger = '<c-c>'
let g:UltiSnipsJumpForwardTrigger = '<c-b>'
let g:UltiSnipsJumpBackwardTrigger = '<c-z>'

"-----------------------------------------------------------------------
"----------------------------------------------------------------vCoolor
"Colores Hex en minúsculas
let g:vcoolor_lowercase = 1

" Deshabilitar asignaciones predeterminadas
let g:vcoolor_disable_mappings = 1

" Reasignación de teclas
let g:vcoolor_map = '<C-E>'
let g:vcool_ins_rgb_map = '<C-R>'		" Insert rgb color.
let g:vcool_ins_hsl_map = '<C-Y>'		" Insert hsl color.
let g:vcool_ins_rgba_map = '<C-G>'		" Insert rgba color.

"-----------------------------------------------------------------------
"----------------------------------------------------------------Rainbow
" Deshabilitar para html
let g:rainbow_conf = {
\	'ctermfgs':  ['brown', 'darkblue', 'darkgray', 'darkgreen', 
\	'darkcyan', 'darkred', 'darkmagenta', 'grey', 'black',
\	'darkmagenta', 'darkblue', 'darkgreen', 'darkcyan', 'darkred', 'red'],
\	'separately': {
\		'html': 0,
\	}
\}

"-----------------------------------------------------------------------
"------------------------------------------------------------Preview UML
" Establecer la URL del servidor plantuml
let g:preview_uml_url='http://localhost:8888'


nnoremap <F7> :silent update<Bar>silent !brave-browser %:p &<CR>
