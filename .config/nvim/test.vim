set number
set mouse=a

call plug#begin()

Plug 'junegunn/vim-plug'
Plug 'jmcantrell/vim-virtualenv', { 'for': 'python' }
Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' } 
Plug 'vim-syntastic/syntastic'
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'lewis6991/gitsigns.nvim'
Plug 'neovim/nvim-lspconfig'
Plug 'nvim-lua/plenary.nvim'
Plug 'famiu/feline.nvim'
Plug 'glepnir/dashboard-nvim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'wfxr/minimap.vim', {'do': ':!cargo install --locked code-minimap'}
Plug 'Yggdroot/indentLine'
Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'ibhagwan/fzf-lua'
Plug 'ibhagwan/nvim-fzf'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
Plug 'akinsho/bufferline.nvim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'aklt/plantuml-syntax'
Plug 'weirongxu/plantuml-previewer.vim'
Plug 'tyru/open-browser.vim'
Plug 'lambdalisue/suda.vim'
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
"Plug 'b3nj5m1n/kommentary'
Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': 'python3 -m chadtree deps'}
Plug 'kyazdani42/nvim-tree.lua'
Plug 'kyazdani42/nvim-web-devicons'

call plug#end()

syntax on
nnoremap <Space> <Nop>
let mapleader = " " 
set termguicolors
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
map <leader>> <C-w>10>
map <leader>< <C-w>10<
map <leader>+ <C-w>5+
map <leader>- <C-w>5-
nmap <leader>s :w<CR>
nmap <leader>qq :qa<CR>
set hlsearch
nmap <silent> ,/ :nohlsearch<CR>
set updatetime=100
set nowrap
au FileType html setl sw=2 sts=2 et
au FileType lua setl sw=2 sts=2 et
nnoremap <F2> :CHADopen<CR>
let g:chadtree_settings = {
			\'theme': {'text_colour_set': "env"},
			\'ignore': {'name_exact': []},
			\'keymap': {'toggle_hidden': ["¿"]}
\}
autocmd BufEnter * if (winnr("$") == 1 && &filetype == "CHADTree") | q | endif
autocmd BufEnter * if (winnr("$") == 1 && &filetype == "CHADTree") | b# | endif
nnoremap <F3> :NvimTreeToggle<CR>
let g:nvim_tree_width = 40
let g:nvim_tree_auto_close = 1
let g:nvim_tree_indent_markers = 1
lua << EOF
require("nvim-web-devicons").setup{}
EOF

lua << EOF
local actions = require 'fzf-lua.actions'
require'fzf-lua'.setup{
\files = {
\	cmd = [[fd --color never --type f --hidden ]] ..
\	[[--exclude .git --exclude node_modules --exclude '*.pyc' --exclude Games]]
\}
\}
\--window_on_create = function()
\--  	vim.cmd("set winhl=Normal:Normal")  -- popup bg to match normal windows
\--   	vim.api.nvim_buf_set_keymap(0, "t", "<Esc>", "<C-c>", { nowait = true, silent = true })
\--end
EOF

" Atajos de teclado
" Abrir buscador de archivos
nnoremap <leader>ff <cmd>lua require('fzf-lua').files()<CR>
" Abrir buscador de archivos para repos git ocultado archivos de .gitignore
nnoremap <leader>fg <cmd>lua require('fzf-lua').git_files()<CR>
" Abrir buscador de buffers
nnoremap <leader>fb <cmd>lua require('fzf-lua').buffers()<CR>
" Abrir buscador de palabras 
nnoremap <leader>fr <cmd>lua require('fzf-lua').grep()<CR>
" Abrir buscador de man pages
nnoremap <leader>fm <cmd>lua require('fzf-lua').man_pages()<CR>

so ~/.config/nvim/plugins/bufferline.lua

nnoremap <leader>bb :BufferLinePick<CR>
let g:dracula_colorterm = 0
colorscheme dracula
set signcolumn=yes
highlight clear SignColumn
lua << EOF
require('gitsigns').setup()
EOF
"so ~/.config/nvim/plugins/feline.lua
let g:minimap_block_filetypes = ['fugitive', 'nerdtree', 'tagbar', 'ChadTree']
let g:minimap_block_buftypes = ['nofile', 'nowrite', 'quickfix', 'terminal', 'prompt', 'fzf', 'pymode']
let g:minimap_close_filetypes = ['starify', 'netrw', 'vim-plug', 'vim-blunde']
let g:minimap_highlight_range = 1
autocmd BufEnter * if bufname('#') =~# "minimap" && winnr('$') > 1 | q | endif
nnoremap <leader>m :MinimapToggle<CR>
nnoremap <leader>mq :MinimapClose<CR>
nnoremap <leader>mr :MinimapRefresh<CR>
let python_highlight_all=1
let g:pymode_doc = 1
let g:pymode_lint_ignore=["E501",]
so ~/.config/nvim/plugins/lsp_config.lua
let g:indentLine_char = "⋮"
nnoremap <leader>i :IndentLinesToggle<CR>
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()
let g:Hexokinase_highlighters = [ 'virtual' ]
nnoremap <leader>h :HexokinaseToggle<CR>
let g:preview_uml_url='http://localhost:8888'
nnoremap <F7> :silent update<Bar>silent !brave-browser %:p &<CR>

